#!/usr/bin/env python3
import os
import sys
import tempfile
from hashlib import sha1
from passhashdb import PasswordDB

class TestFailure(Exception):
    pass


def run_test(passwords, dbname):
    # Write database
    with PasswordDB.open(dbname, 'w') as db:
        hashes = []
        for i, password in enumerate(passwords):
            h = sha1(password.encode('utf-8'))
            hashes.append( (h.digest(), i) )

        hashes.sort()
        for h, count in hashes:
            db.add_hash(h, count)

    # Read
    with PasswordDB.open(dbname, 'r') as db:
        for i, password in enumerate(passwords):
            count = db.search_password(password)
            assert i == count

        try:
            db.search_password('xxxxxnotfoundxxxxxxx')
        except KeyError:
            pass
        else:
            raise TestFailure("Password should not have been found")


def remove_file(path):
    try:
        os.remove(path)
    except FileNotFoundError:
        pass


def main():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('passwordfile', nargs='?',
            type=argparse.FileType('r'), default=sys.stdin)
    args = ap.parse_args()

    passwords = [line.strip() for line in args.passwordfile]

    dbname = tempfile.mktemp()
    try:
        run_test(passwords, dbname)
    finally:
        remove_file(dbname)

    print("OK")

if __name__ == '__main__':
    main()
